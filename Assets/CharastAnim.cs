﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharastAnim : MonoBehaviour
{
    bool jump;
    public Rigidbody rg;
    public bool walk;
    private Animator anim;
    public int jumpForce;
    void Start()
    {
        anim = GetComponent<Animator>();
        rg = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W)|| Input.GetKey(KeyCode.A)|| Input.GetKey(KeyCode.D)|| Input.GetKey(KeyCode.S))
        {
            walk = true;
        }
        else
        {
            walk = false;
        }
        anim.SetBool("Walk", walk);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (jump == false)
            {
                rg.AddForce(Vector3.up * jumpForce * Time.deltaTime, ForceMode.Impulse);
                anim.SetBool("Jump", true);
                jump = true;
            }
        }
    }
    private void OnCollisionEnter(Collision col)
    {
        if (col.collider != null)
        {
            jump = false;
            anim.SetBool("Jump", false);
        }
    }
}
