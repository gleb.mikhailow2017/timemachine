﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WepContrl : MonoBehaviour
{
    float mv;

    public Transform BackSp;
    public Transform PistolSp;
    public Transform ArmSp;

    public GameObject wep1;
    public GameObject wep2;
    public GameObject wep3;

    public WepCheck wc;

    void Start()
    {
        if (PistolSp.GetChild(0) != null) wep1 = PistolSp.GetChild(0).gameObject;
        if (ArmSp.GetChild(0) != null) wep2 = ArmSp.GetChild(0).gameObject;
        if (BackSp.GetChild(0) != null) wep3 = BackSp.GetChild(0).gameObject;
    }

    void Update()
    {
        mv = Input.GetAxis("Mouse ScrollWheel");
        if (mv != 0f)
        {
            wep2.transform.SetParent(wep1.transform.parent);
            if (wep1.transform.parent == PistolSp) wep1.transform.SetParent(ArmSp);
            else wep1.transform.SetParent(PistolSp);
            wep1.transform.localPosition = new Vector3(0f, 0f, 0f);
            wep1.transform.localRotation = Quaternion.identity;
            wep2.transform.localPosition = new Vector3(0f, 0f, 0f);
            wep2.transform.localRotation = Quaternion.identity;
        }
    }
    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.F) && wc.mb == true)
        {
            wep3.GetComponent<Rigidbody>().isKinematic = false;
            wep3.GetComponent<BoxCollider>().enabled = true;
            wep3.transform.SetParent(null);
            wep3 = wc.tmpWep.gameObject;
            wep3.transform.SetParent(BackSp);
            wep3.GetComponent<Rigidbody>().isKinematic = true;
            wep3.GetComponent<BoxCollider>().enabled = false;
            wep3.transform.localPosition = new Vector3(0f, 0f, 0f);
            wep3.transform.localRotation = Quaternion.identity;
        }
    }

}
