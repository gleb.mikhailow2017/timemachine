﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
    public int hp, damage;

    float x;
    float z;
    Rigidbody rg;
    public Transform point1;
    public Transform tmp_point;
    void Search()
    {
        tmp_point = GameObject.FindGameObjectsWithTag("point")[0].transform;
        if (tmp_point == point1)
        {
            tmp_point = GameObject.FindGameObjectsWithTag("point")[1].transform;
        }
    }
    private void Start()
    {
        rg = GetComponent<Rigidbody>();
        tmp_point = GameObject.FindGameObjectsWithTag("point")[0].transform;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "point")
        {
            point1 = other.gameObject.transform;
        }
        Search();
    }
    private void Update()
    {
        rg.velocity = new Vector3(tmp_point.position.x - gameObject.transform.position.x, 0, tmp_point.position.z - gameObject.transform.position.z );
    }
}
