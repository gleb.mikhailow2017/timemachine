﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WepCheck : MonoBehaviour
{
    public GameObject tmpWep;
    public bool mb;

    private void OnTriggerEnter(Collider col)
    {
        if (col != null && col.gameObject.layer == 9)
        {
            mb = true;
            tmpWep = col.gameObject;
        }
        else { tmpWep = null; mb = false; }
    }
}
